from functions import m_json
from functions import m_pck
import os


#Defining paths
dirname = os.path.dirname(__file__)
setup_path = os.path.join(dirname,"datasheets/setup_newton.json")
sheets_path = os.path.join(dirname,"datasheets")
data_path = os.path.join(dirname,"data")

#Collecting metadata
metadata = m_json.get_metadata_from_setup(setup_path)

#Adding sensor serial numbers
sensor_serials = m_json.add_temperature_sensor_serials(sheets_path,metadata)
metadata['sensor']['serials'] = sensor_serials

#Collecting data
data = m_pck.get_meas_data_calorimetry(metadata)

# Log data and archive json files
m_pck.logging_calorimetry(data,metadata,data_path,sheets_path)
m_json.archiv_json(sheets_path,setup_path,data_path)
